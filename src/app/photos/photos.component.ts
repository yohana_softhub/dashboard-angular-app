import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  photos[];

  constructor(private data: DataService) {
    this.data.getPhotos().subscribe(photos => {
      this.photos = photos;
    });
  }

  ngOnInit() {
  }

}
